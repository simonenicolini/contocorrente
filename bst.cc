#include <iostream>
#include <cstring>
using namespace std;

#include "op.h"
#include "bst.h"

/*static tipo_key copy_key(tipo_key& s,tipo_key s1){
	strcpy(s, s1);
	return s;
}*/

/*static tipo_key compare_key(tipo_key s,tipo_key s1){
		return strcmp(s,s1);
}*/

void print_key(tipo_key k){
	cout<<k;
}
/*tipo_key get_key(bnode* n){
	return (n->key);
}*/

tipo_inf get_value(bnode* n){
	return (n->inf);
}

bst get_left(bst t){
	return (t->left);
}

bst get_right(bst t){
	return (t->right);
}

bnode* get_parent(bnode* n){
	return (n->parent);
}

bnode* bst_newNode(tipo_key k, tipo_inf i){
	bnode* n=new bnode;
	copy(n->inf,i);
	strcpy(n->key, k);
	n->right=n->left=n->parent=NULL;
	return n;
}

void bst_insert(bst& b, bnode* n){
	bnode* x;
	bnode* y=NULL;
	if(b==NULL){
		    	b=n;
		    }
	else{
	    x=b;
	    while (x != NULL) {
	      y=x;
	      if (strcmp(n->key,x->key)<0) {
		      x = get_left(x);
	      } else {
		      x = get_right(x);
	      }
	    }
	    n->parent = y;
	    if (strcmp(n->key, y->key)<0) {
	      y->left = n;
	    } else {
	      y->right = n;
	    }
}}

bnode* bst_search(bst b,tipo_key k){

	    while (b != NULL) {
	      if (strcmp(k,b->key)==0)
	    	  return b;
	      if (strcmp(k,b->key)<0) {
		      b = get_left(b);
	      } else {
		      b = get_right(b);
	      }
	    }
	    return NULL;
}
