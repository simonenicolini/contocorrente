/// Struttura dati di una operazione effettuata da un conto corrente

/** ********************************************************************************
 * Ogni operazione registrata sul conto mantiene i seguenti dati.
 * @note Non viene fatto alcun controllo sul tipo di operazione registrata (lettere diverse da
 * B – Bonifico, U – utenza, C – carta).
 * Ogni operazione è il tipo di dato mantentuto dal bst.
 ******************************************************************************** */

typedef struct{
	/** cifra dell'operazione non intera ma decimale */
	float cifra;
	/** indica se l’operazione è a debito o a credito (true se a debito, false altrimenti) */
	bool deb_cred;
	/** tipo dell'operazione effettuata (B – Bonifico, U – utenza, C – carta)  */
	char tipo;
}tipo_inf;

int compare(tipo_inf, tipo_inf);
void copy(tipo_inf&, tipo_inf);
void print(tipo_inf);




